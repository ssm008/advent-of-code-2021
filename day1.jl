include("aocutils.jl")
sonar = read_number_list("input/day1.txt")

@time begin
    prev = sonar[1]
    result1 = 0
    for depth in sonar
        result1 += (depth > prev)
        prev = depth
    end
    print("part1: $(result1)\n")
end 

@time begin
    prevw = sum(sonar[1:3])
    result2 = 0
    for (i, depth) in enumerate(sonar[3:end])
        w = sum(sonar[i:i+2])
        result2 += (w > prevw)
        prevw = w
    end

    print("part2: $(result2)\n")
end
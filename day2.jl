include("aocutils.jl")
command_input = readlines("input/day2.txt")

struct Command
    direction::String
    amount::Int32
end

function Base.:parse(Command, str)
    dir, tmp = split(str, " ")
    amount = parse(Int32, tmp)
    return Command(dir, amount)
end

mutable struct Position
    depth::Int32
    hpos::Int32
    aim::Int32
end

@time begin
    pos = Position(0,0,0)
    for c in command_input
        com = parse(Command, c)
        if com.direction == "forward"
            pos.hpos += com.amount
        elseif com.direction == "up"
            pos.depth -= com.amount
        else
            pos.depth += com.amount
        end
    end
    result1 = pos.depth * pos.hpos
    print("part1: $(result1)\n")
end 

@time begin
    pos = Position(0,0,0)
    for c in command_input
        com = parse(Command, c)
        if com.direction == "forward"
            pos.hpos += com.amount
            pos.depth += pos.aim * com.amount
        elseif com.direction == "up"
            pos.aim -= com.amount
        else
            pos.aim += com.amount
        end
    end
    result2 = pos.depth * pos.hpos
    print("part2: $(result2)\n")
end
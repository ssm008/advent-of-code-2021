# Advent of Code 2021
Learning Julia this year

### Day 1
```
part1: 1681
  0.004050 seconds (6.86 k allocations: 139.469 KiB, 81.93% compilation time)
part2: 1704
  0.088731 seconds (129.63 k allocations: 6.677 MiB, 97.58% compilation time)
```
```
part1: 1681
  0.000898 seconds (6.84 k allocations: 138.391 KiB)
part2: 1704
  0.001859 seconds (29.74 k allocations: 909.953 KiB)
```

### Day 2
```
part1: 1604850
  0.001465 seconds (10.14 k allocations: 424.359 KiB)
part2: 1685186100
  0.001515 seconds (12.70 k allocations: 464.641 KiB)
```

### Day 3
```
part1: 2003336
  0.009410 seconds (86.79 k allocations: 2.807 MiB)
part2: 1877139
  0.149660 seconds (81.10 k allocations: 4.500 MiB, 99.04% compilation time)
```

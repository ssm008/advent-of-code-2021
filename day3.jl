include("aocutils.jl")
diagnostic = readlines("input/day3.txt")

@time begin
    N = length(diagnostic)
    M = length(diagnostic[1])
    diagnostic_sum = zeros(Int32, M)

    for line in diagnostic
        for (i, c) in enumerate(line)
            diagnostic_sum[i] += (c == '1' ? 1 : 0)
        end
    end

    γ = ϵ = 0
    for (i, r) in enumerate(diagnostic_sum)
        if (r > N/2.0)
            γ += 1 << (M-i)
        else
            ϵ += 1 << (M-i)
        end
    end

    result1 = γ * ϵ
    print("part1: $(result1)\n")
end 

@time begin
    # This is the worst code I've made in ages
    function mcn(x, i)
        digits = [parse(UInt8, d[i]) for d in x]
        return sum(digits) >= length(x)/2 ? 1 : 0
    end
    lcn(x, i) = 1 - mcn(x,i)

    M = length(diagnostic[1])
    o2 = co2 = diagnostic # Start with full list for both
    for i in 1:M
        most_common_number_o2 = mcn(o2,i)
        least_common_number_co2 = lcn(co2,i)
        if length(o2) > 1
            o2 = filter(x->parse(UInt8, x[i]) == most_common_number_o2, o2)
        end
        if length(co2) > 1
            co2 = filter(x->parse(UInt8, x[i]) == least_common_number_co2, co2)
        end
    end
    result2 = parse(Int32, "0b"*o2[1]) * parse(Int32, "0b"*co2[1])
    print("part2: $(result2)\n")
end